import requests
import secrets
import random

# Fetching given words from specified url
url = "http://www.mieliestronk.com/corncob_lowercase.txt"
data = requests.get(url).text
words_list = data.split("\r\n") # Splitting txt into a list

# Get user input
print('#################################')
print('###    Welcome to Scramble    ###')
print('#################################')
print()
print('Please enter your phrase:')
input_ = input()

# --- Extracting details from input phrase ---
phrase = dict()
for x in input_.split(" "):
    phrase[x] = {"length":len(x), "first":x[0], "last":x[-1]}
# ---------------------------------------------

error_word = ""         # Variable to store the word for which there are replacements 
words = dict()          # Dict to link each phrase word with the replacement words

replace_length = 58000  # Variable to determine how many phrases will be returned. 
                        # Determined by the least number of replacements for a given word

# Loop through given words to find all possible matching replacements for each given word
for phr in phrase:
    temp_list = list()

    phr_length = phrase[phr]["length"]
    phr_first = phrase[phr]["first"]

    for word in words_list:
        word_length = len(word)
        if(word_length > 0 and word != phr):
            word_first = word[0]

            # Finding a matching word of same length and same first character
            if(word_length == phr_length and word_first == phr_first):
                temp_list.append(word)
            
    # Recording how many unique sentences can be made without repaeting based on the shortest list of replacement words
    list_length = len(temp_list)
    if(replace_length > list_length):
        replace_length = list_length

    # Seperating out the word which has no replacement
    if(len(error_word) < 1 and replace_length == 0):
        error_word = phr
        break

    # Randomising each final list of replacement words
    random.shuffle(temp_list)
    words[phr] = temp_list

if(len(error_word) > 0):
    print("Cannot find replacement for " + error_word)
else:
    # Creating new result phrases by combining the randomised lists
    count = 0
    new_phrases = list()
    for x in range(replace_length):
        temp_phrase = ""
        for phr in phrase:
            temp_phrase = temp_phrase + words[phr][count] + " "
        count += 1
        new_phrases.append(temp_phrase)

    print("\nHere are all the possilbe alternative phrases that could be made:")
    for x in new_phrases:
        print(x)

input("\n--Press enter to end--\n")