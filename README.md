# Scramble #

Scrabble assistant Tool for senetence randomisation and word discovery

### Technologies used

* Python 3.7.4
* Django 3.1.1
* pyinstaller - For creating a redistributable python executable 

### How do I get set up? ###

pip install virtualenv
```
virtualenv -p 3.7 .virtualenv
```
Activate virtual environment at 
/.virtualenv/Scripts/activate
```
pip install -r requirements.txt
```

### How does the customer use it? ###

The customer only needs Scramble.exe and does not require any setup or installations to run the program
Simply run Scramble.exe

### Who do I talk to? ###

* Michael Caloba (Creator) - <michaelcaloba@gmail.com>